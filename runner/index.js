/* eslint-disable no-console */
const readline = require('readline');
const Game = require('../build');

const rl = readline.createInterface(process.stdin, process.stdout);


const printHelp = () => {
  console.log('='.repeat(20));
  console.log('Welcome to play Mark tic tac toe!');
  console.log('Normally, you need to use `new` command to start a game, ' +
    'use `join` command to get every players on board, and use `start` command to start the game');
  console.log('Help about Mark tic tac toe:');
  console.log('+ `new [N]`, to start a new game with optional board\'s row number N');
  console.log('+ `join [name] [symbol]`, to join the game with given name and symbol');
  console.log('+ `start`, start playing this game');
  console.log('+ `take [x] [y]`, to make a move on position (x, y)');
  console.log('+ `restart]`, to restart the game with current players');
  console.log('+ `close`, close Mark tic tac toe game');
  console.log('='.repeat(20));
};


printHelp();
rl.setPrompt('Mark Tic Tac Toe> ');
rl.prompt();

const game = new Game();
let player;

const handleInput = (line) => {
  const params = line.trim().split(' ');

  switch (params[0]) {
    case 'new':
      if (params[1]) {
        game.setBoardRow(Number.parseInt(params[1], 10));
      } else {
        game.setBoardRow(3);
      }

      console.log('Set game board success! Please join game by using `join` command');
      break;
    case 'join':
      if (params.length < 3) {
        console.log('Usage: join [name] [symbol], we will just pick the first character of [symbol] field');
        break;
      }
      const name = params[1].toString();
      const symbol = params[2].charAt(0).toString();
      game.joinGame(name, symbol);
      console.log(`Player ${name} joined with symbol: [${symbol}]!`);
      break;
    case 'start':
      player = game.startGame();
      game.renderInTerminal();
      console.log(`Your turn: ${player.name}, please act by using \`take [x] [y]\` command`);
      break;
    case 'take':
      if (params.length < 3) {
        console.log('Usage: take [x] [y] to make your move!');
        break;
      }
      if (!player) {
        console.error('You need to start the game before taking a move');
        break;
      }
      const nextPlayer = game.makeMove(player, params[1], params[2]);
      game.renderInTerminal();
      if (game.isWin()) {
        console.log('='.repeat(20));
        console.log(`Congratulations! ${player.name} wins the game! Please use \`restart\` command to start a new game.`);
        console.log('='.repeat(20));
        break;
      } else if (game.isDraw()) {
        console.log('Draw! Please use `restart` command to start a new game.');
        break;
      }
      player = nextPlayer;
      console.log(`${player.name}, please take your act by using \`take [x] [y]\` command`);
      break;
    case 'restart':
      player = game.restart();
      game.renderInTerminal();
      console.log(`Your turn: ${player.name}, please act by using \`take [x] [y]\` command`);
      break;
    case 'close':
      rl.close();
      break;
    default:
      printHelp();
      break;
  }
};

rl.on('line', (line) => {
  try {
    handleInput(line);
  } catch (e) {
    console.error(`Error happened! ${e}`);
    console.log('Please retry.');
  }
  rl.prompt();
});

rl.on('close', () => {
  console.log('Bye bye! :D');
  process.exit(0);
});


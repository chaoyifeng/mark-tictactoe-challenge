import { assert } from 'chai';
import Game from '../../src/core/Game';

describe('Game', () => {
  describe('Game could be started', () => {
    it('could be initialized', () => {
      const game = new Game();
      assert.isNotNull(game);
    })

    it('should be able to start with valid parameters', () => {
      const game = new Game();
      game.setBoardRow(3);
      game.joinGame('John', 'X');
      game.joinGame('Betty', 'O');
      assert.notEqual(game.startGame(), false, 'cant start game');
    });

    it('should throw error with invalid board', () => {
      assert.throws(function() {
        const game = new Game();
        game.startGame();
      });
    });

    it('should throw error with invalid players', () => {
      assert.throws(function() {
        const game = new Game();
        game.setBoardRow(3);
        game.startGame();
      });
    });

    it('should throw error with invalid first act', () => {
      assert.throws(function() {
        const game = new Game();
        game.setBoardRow(3);
        game.joinGame('John', 'X');
        game.joinGame('Betty', 'O');
        game.setFirstAct(3);
        // game.startGame();
      });
    });

    it('should be able to start with first player returned', () => {
      const game = new Game();
      game.setBoardRow(3);
      game.joinGame('John', 'X');
      game.joinGame('Betty', 'O');
      game.setFirstAct(1);
      const firstPlayer = game.startGame();
      assert.equal(firstPlayer.name, 'Betty', 'first player incorrect');
    });
  });

  describe('Game could be played by players', () => {
    let game;

    beforeEach(() => {
      game = new Game();
      game.setBoardRow(3);
      game.joinGame('John', 'X');
      game.joinGame('Betty', 'O');
    });


    it('should be able to start with first player returned', () => {
      const firstPlayer = game.startGame();
      let player = game.makeMove(firstPlayer, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 1);
      assert(game.isWin(), 'wrong game state!');
    });

    it('should be able to restart', () => {
      const firstPlayer = game.startGame();
      let player = game.makeMove(firstPlayer, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 1);
      assert(game.isWin(), 'wrong game state!');
      game.restart();
      player = game.makeMove(player, 0, 2);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 1);
      assert(game.isWin(), 'wrong game state!');
    });

    it('should be able to draw', () => {
      const firstPlayer = game.startGame();
      let player = game.makeMove(firstPlayer, 0, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 2);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 2);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 2);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 0);
      assert(game.isDraw(), 'wrong game state!');
    })
  });

  describe('6*6 Game could be played by players', () => {
    let game;

    beforeEach(() => {
      game = new Game();
      game.setBoardRow(6);
      game.joinGame('John', 'X');
      game.joinGame('Betty', 'O');
    });


    it('should be able to start with first player returned', () => {
      const firstPlayer = game.startGame();
      let player = game.makeMove(firstPlayer, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(firstPlayer, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(firstPlayer, 2, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(firstPlayer, 2, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(firstPlayer, 0, 1);
      assert(game.isWin(), 'wrong game state!');
    });

    it('should be able to restart', () => {
      let firstPlayer = game.startGame();
      let player = game.makeMove(firstPlayer, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 1);
      assert(game.isWin(), 'wrong game state!');
      firstPlayer = game.restart();
      player = game.makeMove(firstPlayer, 0, 2);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 1, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 3, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 0);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 0, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 4, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 5, 1);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 4, 3);
      assert(game.isContinue(), 'wrong game state!');
      player = game.makeMove(player, 2, 1);
      assert(game.isWin(), 'wrong game state!');
    });
  });
});

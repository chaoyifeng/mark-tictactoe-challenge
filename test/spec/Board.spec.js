import { assert } from 'chai';
import {Board} from '../../src/core/Board';
import { Player, PlayerGroup } from "../../src/core/Player";

describe('Board', () => {
  describe('Board should be initialized', () => {
    it('should be initialized with empty parameters', () => {
      const board = new Board();
      assert.isNotNull(board);
    });
  });

  describe('Board can be rendered with correct rows and columns', () => {
    it('should get 3*3 board by default', () => {
      const board = new Board();
      const panel = board.getPanel();
      assert.strictEqual(panel.length, 3 + 4 + 1);
    });

    it('should get n*n board if specified row number', () => {
      const board = new Board({ row: 5 });
      const panel = board.getPanel();
      assert.strictEqual(panel.length, 5 + 6 + 1);
    })
  });

  describe('Board can be rendered as expected in consoles', () => {
    it('should correctly render 3*3 board as expected', () => {
      const board = new Board();
      const panel = board.getPanel();
      assert.strictEqual(panel[0].length, panel[1].length, 'They share the same width');
    });

    it('should correctly render n*n board as expected', () => {
      const board = new Board({ row: 5 });
      const panel = board.getPanel();
      assert.strictEqual(panel[0].length, panel[1].length, 'They share the same width');
    });
  });

  describe('Board can update its state according to players move', () => {
    let board, player1, player2;

    beforeEach(() => {
      board = new Board();
      player1 = new Player({ name: 'Jack', symbol: 'X' });
      player2 = new Player({ name: 'Tom', symbol: 'O' });
    });

    it('can let player make valid move', () => {
      assert.doesNotThrow(() => {
        board.updateState({ x: 1, y: 1 }, player1);
        board.updateState({ x: 1, y: 2 }, player2);
        board.updateState({ x: 0, y: 2 }, player1);
      });
    });

    it('would throw if player make invalid move', () => {
      assert.throws(() => {
        board.updateState({ x: 1, y: 1 }, player1);
        board.updateState({ x: 1, y: 1 }, player2);
      });

      assert.throws(() => {
        board.updateState({ x: 1, y: 1 }, player1);
        board.updateState({ x: 1, y: 3 }, player2);
      });

      assert.throws(() => {
        board.updateState({ x: 1, y: 1 }, player1);
        board.updateState({ x: 2, y: 1 }, player2);
        board.updateState({ x: 2, y: 1 }, player1);
      });
    });

    it('would update rendered board correctly after a valid move', () => {
      let panel = board.getPanel();
      assert.strictEqual(panel[0].length, panel[1].length, 'They share the same width');
      board.updateState({ x: 0, y: 1 }, player1);
      panel = board.getPanel();
      assert.strictEqual(panel[0].length, panel[1].length, 'They share the same width');
      assert(panel[2].indexOf(player1.symbol) > -1, 'Cant find symbol in first line');

      board.updateState({ x: 1, y: 1 }, player2);
      panel = board.getPanel();
      assert.strictEqual(panel[0].length, panel[3].length, 'They share the same width');
      assert(panel[4].indexOf(player2.symbol) > -1, 'Cant find symbol in third line');
    })
  });

  describe('Board will get a current result after a player makes his move', () => {
    let board, playerGroup;

    beforeEach(() => {
      board = new Board();
      const player1 = new Player({ name: 'Jack', symbol: 'X' });
      const player2 = new Player({ name: 'Tom', symbol: 'O' });
      playerGroup = new PlayerGroup();
      playerGroup.addPlayer(player1);
      playerGroup.addPlayer(player2);
      playerGroup.setFirstPlayerIndex(1); // let Tom act first
    });

    it('would return CONTINUING result when game is still going', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 2 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.CONTINUING);
    });

    it('would return DRAW result when all grids have been filled', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.DRAW);
    });

    it('would return WIN result when the acting player won the game', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });

    it('would return WIN result when the acting player won the game - 2', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 0 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });

    it('would return WIN result when the acting player won the game - 3', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });

    it('would return WIN result when the acting player won the game - 4', () => {
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.CONTINUING);
      board.updateState({ x: 2, y: 2 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });
  });



  describe('A larger board will get a current result after a player makes his move', () => {
    let board, playerGroup;

    beforeEach(() => {
      board = new Board({row: 6});
      const player1 = new Player({ name: 'Jack', symbol: 'X' });
      const player2 = new Player({ name: 'Tom', symbol: 'O' });
      playerGroup = new PlayerGroup();
      playerGroup.addPlayer(player1);
      playerGroup.addPlayer(player2);
      playerGroup.setFirstPlayerIndex(1); // let Tom act first
    });

    it('would return CONTINUING result when game is still going', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 2 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.CONTINUING);
    });

    it('would return WIN result when the acting player won the game', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });

    it('would return WIN result when the acting player won the game - 2', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 0 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });

    it('would return WIN result when the acting player won the game - 3', () => {
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 2 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });

    it('would return WIN result when the acting player won the game - 4', () => {
      board.updateState({ x: 0, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 1 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 1, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 0, y: 0 }, playerGroup.nextActivePlayer());
      board.updateState({ x: 2, y: 1 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.CONTINUING);
      board.updateState({ x: 2, y: 2 }, playerGroup.nextActivePlayer());
      assert.strictEqual(board.getResult(), board.resultSet.WIN);
    });
  });


});

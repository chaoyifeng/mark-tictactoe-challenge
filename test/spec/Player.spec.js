import { assert } from 'chai';
import {Player, PlayerGroup} from '../../src/core/Player';

describe('Player', () => {
  describe('Player could be initialized', () => {
    it('should be initialized with valid parameters', () => {
      const player = new Player({
        name: 'Jack',
        symbol: 'X',
      });
      assert.isNotNull(player);
    });

    it('should throw error with invalid parameters', () => {
      assert.throws(function() {
        new Player();
      });
    });
  });
});

describe('PlayerGroup', () => {
  it('could let valid players join a group', () => {
    const group = new PlayerGroup();
    group.addPlayer(new Player({name: 'Jack', symbol: 'X'}));
    group.addPlayer(new Player({name: 'Tom', symbol: 'O'}));
    assert.strictEqual(group.size(), 2, 'group size mismatch');
    group.addPlayer(new Player({name: 'Mike', symbol: 'T'}));
    assert.strictEqual(group.size(), 3, 'group size mismatch');
  });

  it('should throw error when duplicate name player join a group', () => {
    assert.throws(function() {
      const group = new PlayerGroup();
      group.addPlayer(new Player({name: 'Jack', symbol: 'X'}));
      group.addPlayer(new Player({name: 'Jack', symbol: 'O'}));
    });
  });

  it('should throw error when duplicate symbol player join a group', () => {
    assert.throws(function() {
      const group = new PlayerGroup();
      group.addPlayer(new Player({name: 'Jack', symbol: 'X'}));
      group.addPlayer(new Player({name: 'Tom', symbol: 'X'}));
    });
  });

  it('should throw error when invalid player join group', () => {
    assert.throws(function() {
      const group = new PlayerGroup();
      group.addPlayer(new Player({name: 'Jack', symbol: 'X'}));
      group.addPlayer({name: 'Tom'});
    });
  });

  it('should throw when set invalid first player index', () => {
    const group = new PlayerGroup();
    group.addPlayer(new Player({name: 'Jack', symbol: 'X'}));
    group.addPlayer(new Player({name: 'Tom', symbol: 'O'}));
    assert.throws(() => {
      group.setFirstPlayerIndex(-1);
    });
    assert.throws(() => {
      group.setFirstPlayerIndex(2);
    });
    assert.throws(() => {
      group.setFirstPlayerIndex('1');
    });
  });

  it('should be about to set first player to act', () => {
    const group = new PlayerGroup();
    group.addPlayer(new Player({name: 'Jack', symbol: 'X'}));
    group.addPlayer(new Player({name: 'Tom', symbol: 'O'}));
    group.setFirstPlayerIndex(0);
    assert.equal(group.nextActivePlayer().name, 'Jack');
    assert.equal(group.nextActivePlayer().name, 'Tom');
    assert.equal(group.nextActivePlayer().name, 'Jack');
    assert.equal(group.nextActivePlayer().name, 'Tom');
  });
});

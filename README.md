# Mark’s Tic tac toe

### How to play
![docker-mark-tictactoe](https://user-images.githubusercontent.com/3454734/48974744-b90c7a80-f09b-11e8-97d3-10343044c2c3.gif)

#### Method 1: Use Docker to run this app
###### Prerequisites
You have Docker service installed and running  
###### Command
Just `docker run -it markselby9/mark-tictactoe`, and you can start playing.


#### Method 2: Play in local environment
###### Prerequisites
installed nodejs or yarn in local env.  
###### Command
After running `yarn` or `npm install`, just run `npm run play` in terminal to play the game!

### Command in the terminal
When playing Mark tic tac toe in terminal, you may use these commands:
+ `new [N]`, to start a new game with optional board's row number N
+ `join [name] [symbol]`, to join the game with given name and symbol
+ `start`, start playing this game
+ `take [x] [y]`, to make a move on position (x, y)
+ `restart]`, to restart the game with current players
+ `close`, close Mark tic tac toe game

### Core API of core package
- `constructor` - Constructor of Game class
- `setBoardRow(row)` - Set the row of board, will set to 3 if given row < 3
- `joinGame(name, symbol)` - Let a player join the game, with name and symbol given
- `setFirstAct(index)` - Optional, set the index of first player to act
- `startGame()` - Start the game, would throw error if some conditions don't meet. Return the first acting player object.
- `makeMove(player, x, y)` - Let a player make a move on point (x, y)
- `restart()` - Restart the game 
- `renderInTerminal()` - Draw current board in terminal
- `isWin(), isDraw(), isContinue()` - query the current game result 

### Commands
- `npm run play` - Play the game!
- `npm run clean` - Remove `lib/` directory
- `npm test` - Run tests with linting and coverage results.
- `npm test:only` - Run tests without linting or coverage.
- `npm test:watch` - You can even re-run tests on file changes!
- `npm test:prod` - Run tests with minified code.
- `npm run lint` - Run ESlint with airbnb-config
- `npm run cover` - Get coverage report for your code.
- `npm run build` - Babel will transpile ES6 => ES5 and minify the code.
- `npm run prepublish` - Hook for npm. Do all the checks before publishing your module.

### Highlights
- Test Driven Development
- Write tests before code so that their behaviour is specified through unit tests
- Scalability, support N*N panel, with M players (however too many players seem crowded for tic tac toe)
- A core package to include and only include game logic, with an interface in terminal
- Dockerized this app, you can play it through a Docker image

### Code practice:
- ES6 + babel
- eslint
- test (mocha + chai)
- git, git flow

### Reference
- I used the boilerplate at: https://github.com/markselby9/npm-module-boilerplate to bootstrap this project

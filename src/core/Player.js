class Player {
  constructor(options = {}) {
    // const defaults = { name: 'Player', symbol: 'X' };
    // const { name, symbol } = Object.assign(defaults, options);
    const { name, symbol } = options;

    if (!name || typeof name !== 'string') {
      throw new Error('Invalid name! Please check');
    }
    if (!symbol || typeof symbol !== 'string') {
      throw new Error('Invalid symbol! Please check');
    }
    this.name = name;
    this.symbol = symbol;
  }
}

class PlayerGroup {
  constructor() {
    this.group = []; // use an Array of Player objects to maintain players
    this.activeIndex = 0; // to indicate the next active player
  }

  addPlayer(player) {
    if (!player || !player.name || !player.symbol) {
      throw new Error('Invalid player!');
    }

    this.group.forEach(({ name, symbol }) => {
      if (name === player.name) {
        throw new Error('Duplicate name found, please rename!');
      }
      if (symbol === player.symbol) {
        throw new Error('Duplicate symbol found, please use another symbol!');
      }
    });

    // valid, add player into set
    this.group.push(player);
  }

  size() {
    return this.group.length;
  }

  setFirstPlayerIndex(index) {
    if (!Number.isInteger(index) || index < 0 || index >= this.size()) {
      throw new Error('Invalid index! Please check.');
    }
    this.activeIndex = index;
  }

  nextActivePlayer() {
    const nextActivePlayer = this.group[this.activeIndex];
    this.activeIndex += 1;
    this.activeIndex %= this.size();
    return nextActivePlayer;
  }
}

export default {
  Player,
  PlayerGroup,
};

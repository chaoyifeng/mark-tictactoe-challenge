import { Board } from './Board';
import { Player, PlayerGroup } from './Player';

import {
  CONTINUING,
  DRAW,
  WIN,
  // DEFAULT_ROW,
  // WINNING_COUNT,
} from './const';

class Game {
  constructor() {
    this.board = null;
    this.playerGroup = new PlayerGroup();
  }

  setBoardRow(row) {
    this.board = new Board({ row });
  }

  joinGame(playerName, symbol) {
    const newPlayer = new Player({
      name: playerName,
      symbol,
    });
    this.playerGroup.addPlayer(newPlayer);
  }

  setFirstAct(index) {
    this.playerGroup.setFirstPlayerIndex(index);
  }

  startGame() {
    if (this.board == null) {
      throw new Error('Can not start game, board not initialized!');
    }
    if (this.playerGroup.size() < 2) {
      throw new Error('Can not start game, not enough players!');
    }
    return this.playerGroup.nextActivePlayer();
  }

  // return the next active player
  makeMove(player, x, y) {
    this.board.updateState({ x, y }, player);
    return this.playerGroup.nextActivePlayer();
  }

  restart() {
    this.board.resetState();
    return this.startGame();
  }

  renderInTerminal() {
    this.board.renderPanel();
  }

  // methods for getting result
  isWin() {
    return this.board.getResult() === WIN;
  }

  isContinue() {
    return this.board.getResult() === CONTINUING;
  }

  isDraw() {
    return this.board.getResult() === DRAW;
  }
}

export default Game;

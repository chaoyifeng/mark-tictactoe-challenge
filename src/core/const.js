
const CONTINUING = 'CONTINUING';
const DRAW = 'DRAW';
const WIN = 'WIN';
const DEFAULT_ROW = 3;
const WINNING_COUNT = 3; // by default, whoever reaches 3 in row wins the game

export default {
  CONTINUING,
  DRAW,
  WIN,
  DEFAULT_ROW,
  WINNING_COUNT,
};

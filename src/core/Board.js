/* eslint-disable no-underscore-dangle */
import {
  CONTINUING,
  DRAW,
  WIN,
  DEFAULT_ROW,
  // WINNING_COUNT,
} from './const';

function Point(x, y) {
  this.x = x;
  this.y = y;
}

class Board {
  constructor(options = {}) {
    const defaults = { row: DEFAULT_ROW };
    const { row } = Object.assign(defaults, options);

    // check valid parameters
    if (row < DEFAULT_ROW) {
      // eslint-disable-next-line no-console
      console.warn(`Can not draw board less than ${DEFAULT_ROW} rows, set ${DEFAULT_ROW} rows instead.`);
      this.row = DEFAULT_ROW;
    } else {
      this.row = Math.floor(row); // as a int
    }

    this.resultSet = {
      CONTINUING,
      WIN,
      DRAW,
    };

    this.resetState();
  }

  resetState() {
    this.state = Array(this.row).fill().map(() => Array(this.row).fill(null));
    this.pointMap = Array(this.row).fill().map(() => Array(this.row).fill(null));
    // to maintain point instances with each position
    for (let x = 0; x < this.row; x += 1) {
      for (let y = 0; y < this.row; y += 1) {
        this.pointMap[x][y] = new Point(x, y);
      }
    }
    this.result = this.resultSet.CONTINUING;
  }

  updateState({ x, y }, player) {
    // check parameters
    if (x === undefined || x < 0 || x >= this.row) {
      throw Error('Invalid x!');
    }
    if (y === undefined || y < 0 || y >= this.row) {
      throw Error('Invalid y!');
    }
    if (!player) {
      throw Error('Invalid player!');
    }

    const point = this.pointMap[x][y];
    if (this.getPoint(point) != null) {
      throw Error('Can not take used position!');
    }
    this.setPoint(point, player);
    this.updateResult(x, y, player); // check whether this player has won the game, or draw?
  }

  getPanel() {
    const _drawNumberX = () => {
      let numberXString = '  ';
      for (let i = 0; i < this.row; i += 1) {
        numberXString += `| ${i} `;
      }
      return `${numberXString}|`;
    };
    const _drawLine = () => '-'.repeat((this.row * 4) + 3);
    const _drawSymbol = (lineNumber) => {
      const line = this.state[lineNumber];
      return `${lineNumber} ${(line.map((pos) => {
        if (pos === null) {
          return '|   ';
        }
        return `| ${pos.symbol} `;
      })).join('')}|`;
    };
    const panel = [];
    panel.push(_drawNumberX());
    for (let i = 0; i < this.row; i += 1) {
      panel.push(_drawLine());
      panel.push(_drawSymbol(i));
    }
    panel.push(_drawLine());
    return panel;
  }

  renderPanel() {
    // draw method to render panel in console
    const panel = this.getPanel();
    // eslint-disable-next-line no-console
    console.log();
    // eslint-disable-next-line no-console
    panel.forEach(p => console.log(p));
    // eslint-disable-next-line no-console
    console.log();
  }

  updateResult(pointX, pointY, thisPlayer) {
    // win?
    // becaue only this action on point [x,y] matters, we just need to search from this point,
    // and check whether there is a winning condition exists
    const pathSet = new Set(); // use _bfs method to find all valid points connected to the point
    const _bfs = (x, y, player, row) => {
      if (x < 0 || x >= row) {
        return false;
      }
      if (y < 0 || y >= row) {
        return false;
      }
      const point = this.pointMap[x][y];
      if (pathSet.has(point)) {
        return false;
      }
      if (this.getPoint(point) !== player) {
        return false;
      }
      pathSet.add(point);
      const neighbors = [];
      for (let xx = x - 1; xx <= x + 1; xx += 1) {
        for (let yy = y - 1; yy <= y + 1; yy += 1) {
          if (xx !== x || yy !== y) {
            if (this.pointMap[xx] && this.pointMap[xx][yy] && !pathSet.has(this.pointMap[xx][yy])) {
              neighbors.push(this.pointMap[xx][yy]);
            }
          }
        }
      }

      neighbors.forEach((npoint) => {
        _bfs(npoint.x, npoint.y, player, row);
      });
      return true;
    };

    const checkPaths = (row, pMap) => {
      const points = [...pathSet];
      points.sort((a, b) => {
        if (a.x !== b.x) return a.x - b.x;
        return a.y - b.y;
      });
      // check whether there is a path has a straight line
      return points.some((p) => {
        const tx = p.x;
        const ty = p.y;
        if (tx < 0 || ty < 0) {
          return false;
        }
        if (tx < row - 2 && pathSet.has(pMap[tx + 1][ty])
          && pathSet.has(pMap[tx + 2][ty])) { // column
          return true;
        }
        if (ty < row - 2 && pathSet.has(pMap[tx][ty + 1])
          && pathSet.has(pMap[tx][ty + 2])) { // row
          return true;
        }
        return !!(tx < row - 2 && ty < row - 2 && pathSet.has(pMap[tx + 1][ty + 1])
          && pathSet.has(pMap[tx + 2][ty + 2])); // diagonal
      });
    };

    // find all valid paths from this point
    _bfs(pointX, pointY, thisPlayer, this.row);

    if (checkPaths(this.row, this.pointMap)) {
      this.result = this.resultSet.WIN; // this player wins this game
      return;
    }
    // draw?
    if (!this.state.some(line => line.some(element => element === null))) {
      this.result = this.resultSet.DRAW; // draw as all grids are occupied;
      return;
    }
    this.result = this.resultSet.CONTINUING;
  }

  getResult() {
    return this.result;
  }

  getPoint(point) {
    return this.state[point.x][point.y];
  }

  setPoint(point, value) {
    this.state[point.x][point.y] = value;
  }
}

export default {
  Board
};
